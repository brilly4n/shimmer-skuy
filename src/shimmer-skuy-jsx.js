{
  "shimmer": {
    "padding": "3px",
    "marginBottom": "10%"
  },
  "shimmer_on_10": {
    "width": "100%",
    "padding": "10px",
    "background": "#e6e8ed"
  },
  "shimmer_on_9": {
    "width": "90%",
    "padding": "10px",
    "background": "#e6e8ed"
  },
  "shimmer_on_8": {
    "width": "80%",
    "padding": "10px",
    "background": "#e6e8ed"
  },
  "shimmer_on_7": {
    "width": "70%",
    "padding": "10px",
    "background": "#e6e8ed"
  },
  "shimmer_on_6": {
    "width": "60%",
    "padding": "10px",
    "background": "#e6e8ed"
  },
  "shimmer_on_5": {
    "width": "50%",
    "padding": "10px",
    "background": "#e6e8ed"
  },
  "shimmer_on_4": {
    "width": "40%",
    "padding": "10px",
    "background": "#e6e8ed"
  },
  "shimmer_on_3": {
    "width": "30%",
    "padding": "10px",
    "background": "#e6e8ed"
  },
  "shimmer_on_2": {
    "width": "20%",
    "padding": "10px",
    "background": "#e6e8ed"
  },
  "shimmer_on_1": {
    "width": "10%",
    "padding": "10px",
    "background": "#e6e8ed"
  },
  "hg_10": {
    "height": "100px"
  },
  "hg_9": {
    "height": "90px"
  },
  "hg_8": {
    "height": "80px"
  },
  "hg_7": {
    "height": "70px"
  },
  "hg_6": {
    "height": "60px"
  },
  "hg_5": {
    "height": "50px"
  },
  "hg_4": {
    "height": "40px"
  },
  "hg_3": {
    "height": "30px"
  },
  "hg_2": {
    "height": "20px"
  },
  "hg_1": {
    "height": "10px"
  },
  "hg_05": {
    "height": "1px"
  },
  "circle": {
    "borderRadius": "50%"
  },
  "top_down": {
    "marginBottom": "3%"
  },
  "top": {
    "marginTop": "3%"
  },
  "on": {
    "WebkitAnimationDuration": "1s",
    "WebkitAnimationFillMode": "forwards",
    "WebkitAnimationIterationCount": "infinite",
    "WebkitAnimationName": "placeHolderShimmer",
    "WebkitAnimationTimingFunction": "linear",
    "background": "#f6f7f9",
    "backgroundImage": "linear-gradient(to right, #f6f7f9 0%, #e9ebee 20%, #f6f7f9 40%, #f6f7f9 100%)",
    "backgroundRepeat": "no-repeat",
    "backgroundSize": "800px 104px",
    "position": "relative"
  }
}